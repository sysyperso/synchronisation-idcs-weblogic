import sys
import wlstModule as wlst
import socket
from socket import gethostname
from socket import getfqdn
import java
import jarray
import java.lang.System
import os
import utils

def addJarToClasspath(s):
        #############################################
        # Purpose: If adding a file/jar call this first
        #       with s = path_to_jar
        #############################################
                # make a URL out of 's'
   f = java.io.File (s)
   u = f.toURL ()
   parameters = jarray.array([java.net.URL], java.lang.Class)
   sysloader =  java.lang.ClassLoader.getSystemClassLoader()
   sysclass = java.net.URLClassLoader
   method = sysclass.getDeclaredMethod("addURL", parameters)
   method.setAccessible(1)
   jar_a = jarray.array([u], java.lang.Object)
   b = method.invoke(sysloader, jar_a)
   return b

def update_sec_config_mbean(value):
   wlst.edit()
   wlst.startEdit()
   wlst.cd('/SecurityConfiguration/bi')
   wlst.set('ClearTextCredentialAccessEnabled',value)
   wlst.save()
   wlst.activate()


def get_wls_pwd(user, pwd, port):
   print 'retrieving Bootstrap Credential'
   wlst.connect(user, pwd,'t3://'+getfqdn()+':'+port)
   update_sec_config_mbean('True')
   domainName = wlst.cmo.getName()
   wlst.cd('/EmbeddedLDAP/bi')
   credential = wlst.cmo.getCredential()
   update_sec_config_mbean('False')
   wlst.disconnect()
   print ' Bootstrap Credential retrieved successfully'
   return credential

class WlsLdapUtil:
    def __init__(self, options):
	self._port = '7001'
	self._domain = 'bi'
	self._filename =  None
	self._user=  None
	self._password = None
	self._domain_home= '/scratch/u01/bics12c/bics/user_projects/domains/bi'
	self._prod_home = None
	self._jps_out = None
	self._oracle_common = None
	self._outdir = None
	for opt, arg in options:
            if opt in ('-u', '--user'):
		self._user  = arg
		#self._password = self.getpassword('Enter password for '+ self._user + ':')
            elif opt in ('-w', '--password'):
		self._password = arg
            elif opt in ('-f', '--in_file'):
		self._filename  = arg
            elif opt in ('-d','--domain_home'):
		self._domain_home =  arg
            elif opt in  ('-o', '--port'):
		self._port = arg
            elif opt in ('-p','--prod_home'):
		self._prod_home = arg
            elif opt in ('-c','--oracle_common'):
		self._oracle_common = arg
            elif opt in ('-D','--outdir'):
		self._outdir = arg


	#   validate mandatory arselfgs
	if self._user is None or self._password is None or self._filename is None or self._prod_home is None or self._oracle_common is None or self._outdir is None:
            print 'Required parameters not passed, refer to script usage'
            sys.exit(1)

	self.group_entries = ['cn', 'description', 'uniquemember']

	self.mandatory_user_entries = ['uid', 'sn', 'givenname', 'displayname', 'mail']
	self.user_entries = self.mandatory_user_entries[:]
	self.user_entries.append('wlsMemberOf')

	self.group_membership = {}

    def get_user(self, user_array):
        from java.util import HashMap
        from  oracle.bi.security.centaurus import DirectoryUser
        from  oracle.bi.security.centaurus.DirectoryUser import OptionalAttributes

        map = HashMap()
        map.put(OptionalAttributes.DISPLAYNAME, user_array[1])
        #map.put(OptionalAttributes.DESCRIPTION, 'desc')
        map.put(OptionalAttributes.MAIL, user_array[5])
        #DirectoryUser(<uid>, <first_name> , <last_name>, map)
        dir_user = DirectoryUser(user_array[0], user_array[3] , user_array[4], map)
        return dir_user




    def prepareJPSConfig(self):
        print 'Preparing jps config file...'
        wls_cred = get_wls_pwd(self._user, self._password, self._port)
        import os
        script_dir  = os.path.dirname(os.path.realpath(sys.argv[0]))
        jps_template = script_dir +'/jps_wls_template'
        self._jps_out = script_dir + '/jps-config-jse_wls_out.xml'
        in_file= open(jps_template,'r')
        filedata = in_file.read()
        filedata = filedata.replace('<WLS_CRED>', wls_cred)
        filedata = filedata.replace('<LOCALHOST:7001>', getfqdn()+':' + self._port)
        out_file = open(self._jps_out, 'w')
        out_file.write(filedata)
        in_file.close()
        out_file.close()
        print 'jps config file created successfully'


    def prepareEnv(self):
        self.prepareJPSConfig()
        # add required jars to classpath
        addJarToClasspath(self._prod_home + '/bi/modules/oracle.bi.security/bi-security-core.jar')
        addJarToClasspath(self._prod_home + '/bi/modules/oracle.bi.security/bi-security-manifest.jar')
        addJarToClasspath(self._prod_home + '/oracle_common/modules/oracle.jps/jps-manifest.jar')

        #set env variables
        java.lang.System.setProperty('domain.home', self._domain_home)
        java.lang.System.setProperty('oracle.security.jps.config',self._jps_out)
        java.lang.System.setProperty('common.components.home',self._prod_home + '/oracle_common')
        print 'Env properties set successfully'


    def export_ldif(self):
        out_ldif_file  =  self._outdir + '/wls_users_groups.ldif'
        try:
            wlst.connect(self._user, self._password,'t3://'+ getfqdn() + ':' + str(self._port))
            wlst.serverConfig()
            cmo = wlst.cd('/SecurityConfiguration/'+self._domain+'/Realms/myrealm/AuthenticationProviders/DefaultAuthenticator')
            from java.util import Properties
            empty_props = Properties()
            #export only users and groups. for others first param will change to corresponding  format
            cmo.exportData('DefaultAtn',out_ldif_file, empty_props)
            print 'WLS users and groups exported successfully'
            wlst.disconnect()
        except:
            print('Exeption in exportData...')
            raise

    def import_into_embedded(self):
        if self._to_import == 'users':
            self.import_users()
        elif self._to_import  == 'groups':
            self.import_groups()
        else:
            raise 'incorret import target '
    
    def import_users(self, user_list):
        #User ID,Display Name,Password
        if len(user_list) == 0:
            print 'No users to delete'
            return
        print 'Starting import of users....'

        self._jps_out= self._domain_home + '/config/fmwconfig/jps-config-jse_wls_out.xml'
        self.prepareEnv()
        from  oracle.bi.security.centaurus import IDSDirectoryService
        import array
        try:
            wlst.connect(self._user, self._password, 't3://' + gethostname() + ':' + self._port)
            wlst.serverConfig()
            cmo = wlst.cd('/SecurityConfiguration/'+self._domain+'/Realms/myrealm/AuthenticationProviders/DefaultAuthenticator')
            for userdata in user_list:     # we read all lines into a list and iterate over it
                if len(userdata) ==6:
                    try:
                        print(userdata)
                        if cmo.userExists(userdata[0]):
                            print 'User already exists.: '+ userdata[0]
                        else:
                            cmo.createUser(userdata[0],utils.generatePassword(),userdata[1])
                            dir_user = self.get_user(userdata)
                            directoryHelper = IDSDirectoryService()
                            directoryHelper.updateUser(dir_user)
                            print 'User created successfully :'+ userdata[0]
                    except :
                            print 'Exception in updating user: '+userdata[0]
                            raise
                else:
                    print 'Ignored invalid record :'+line
            if self._jps_out is not None and os.path.isfile(self._jps_out):
                os.remove(self._jps_out)
        except :
            print 'Exception in importing users...'
            raise


    def import_groups(self, groups_list):
        if len(groups_list) == 0:
            print 'No users in groups to delete'
            return

        #Display Name,Description,User Members
        print 'Starting import of groups and members....'
        try:
            wlst.connect(self._user, self._password, 't3://' + gethostname() + ':' + self._port)
            wlst.serverConfig()
            cmo = wlst.cd('/SecurityConfiguration/'+self._domain+'/Realms/myrealm/AuthenticationProviders/DefaultAuthenticator')
            grp_member = {}
            for userdata in groups_list:     # we read all lines into a list and iterate over it
                if len(userdata) == 3:
                    try:
                        cmo.createGroup(userdata[0],userdata[1])
                        print 'Created group: '+ userdata[0]

                    except :
                        exc_type, exc_value, exc_traceback = sys.exc_info()
                        if ignore_wls_err in str(exc_value):
                            print 'Group already exists: '+userdata[0]
                            exc_type, exc_value, exc_traceback = sys.exc_info()
                        else:
                            raise
                    if userdata[2]:
                        if grp_member.has_key(userdata[0]):
                            grp_member[userdata[0]].append(userdata[2])
                        else:
                            grp_member.setdefault(userdata[0],[userdata[2]])

                else:
                    print 'Ignored invalid record :'+line
            # add members to group
            for group, members in grp_member.iteritems():
                print 'Adding following members to group: '+ group + '\nMemebers list: '+''.join(members)
                for member in members:
                    members_list = member.rstrip().split(';')
                    for a_member in members_list:
                        print 'Adding member '+ a_member + ' to group '  + group
                        cmo.addMemberToGroup(group,a_member)
        except :
            print 'Exception in importing groups...'
            raise

    def delete_users(self, user_list):
        #User ID
        if len(user_list) == 0:
            print 'No users to delete'
            return
        print 'Starting deletion of users....'
        try:
            wlst.connect(self._user, self._password,'t3://'+socket.getfqdn()+':'+self._port)
            wlst.serverConfig()
            cmo = wlst.cd('/SecurityConfiguration/'+self._domain+'/Realms/myrealm/AuthenticationProviders/DefaultAuthenticator')
            for userdata in user_list:     # we read all lines into a list and iterate over it
                if len(userdata) >= 1:
                    try:
                        if cmo.userExists(userdata[0]) :
                            cmo.removeUser(userdata[0])
                            print 'Removed user: '+ userdata[0]
                        else:
                            print 'User does not exists: '+userdata[0]
                    except :
                        print 'Exception in removing user...'+ userdata[0]
                        raise
        except :
            print 'Exception in removing users...'
            raise

    def remove_members(self, cmo, group, members):
        if cmo.groupExists :
           for member in members:
               print 'Removing member '+ member + ' from group '  + group
               cmo.removeMemberFromGroup(group, member)


    def get_groups_members(self,cmo,group):
        grp_member = []
        if cmo.groupExists :
            out = cmo.listGroupMembers(group,'*',100)
            while cmo.haveCurrent(out) :
                mem = cmo.getCurrentName(out)
                grp_member.append(mem)
                cmo.advance(out)
            cmo.close(out)
            print 'Existing group members :'+','.join(grp_member)
        return grp_member

    def sync_groups(self, grp_member):
        wlst.connect(self._user, self._password,'t3://'+socket.getfqdn()+':'+self._port)
        wlst.serverConfig()
        cmo = wlst.cd('/SecurityConfiguration/'+self._domain+'/Realms/myrealm/AuthenticationProviders/DefaultAuthenticator')
        for group, members in grp_member.iteritems():
            members = members[0]
            print 'Updating following members to group: '+ group + '\nMembers list: '+','.join(members)
            if cmo.groupExists :
                existing_members  = self.get_groups_members(cmo, group)
                from sets import Set
                to_be_removed = list (Set(existing_members) - Set(members))
                self.remove_members(cmo,  group, to_be_removed)
                to_be_added  = list(Set(members) - Set(existing_members))
                self.add_members(cmo, group, to_be_added)
        wlst.disconnect()

    def add_members(self, cmo,group,members):
        if len(members) == 0 or members == ['']:
            return
        if cmo.groupExists :
           for member in members:
               print 'Adding member '+ member + ' to group '  + group
               cmo.addMemberToGroup(group, member)

    def update_groups(self, group_list):
        if len(group_list) == 0:
            print 'No group to update'
            return
        #Display Name,Description,User Members
        print 'Starting updation of groups ....'
        try:
            grp_member = {}
            for userdata in group_list:     # we read all lines into a list and iterate over it
                userdata[2] = str(userdata[2]).split(';')
                if len(userdata) == 3:
                    if not grp_member.has_key(userdata[0]):
                        grp_member.setdefault(userdata[0],[userdata[2]])
                    elif userdata[2]:
                        grp_member[userdata[0]].append(userdata[2])

                else:
                    print 'Ignored invalid record :'+line
            # add members to group
            self.sync_groups(grp_member)

        except :
            print 'Exception in updating groups...'
            raise

    def update_users(self, users_list):
        if len(users_list) == 0:
            print 'No users to update'
            return
        #User ID,Display Name,Password,givenname,mail
        print 'Starting updation of users....'
        self.prepareEnv()
        from  oracle.bi.security.centaurus import IDSDirectoryService
        import array
        try:
            wlst.connect(self._user, self._password,'t3://'+socket.getfqdn()+':'+self._port)
            wlst.serverConfig()
            cmo = wlst.cd('/SecurityConfiguration/'+self._domain+'/Realms/myrealm/AuthenticationProviders/DefaultAuthenticator')
            for userdata in users_list:     # we read all lines into a list and iterate over it
                if len(userdata) ==6:
                    try:
                        if cmo.userExists(userdata[0]):
                            dir_user = self.get_user(userdata)
                            directoryHelper = IDSDirectoryService()
                            directoryHelper.updateUser(dir_user)
                            #pwd = array.array('c',userdata[2])
                            #directoryHelper.resetUserPassword(userdata[0],pwd)
                            print 'User updated successfully :'+ userdata[0]
                        else:
                            print 'User does not exists skipping update for user: '+ userdata[0]
                    except :
                            print 'Exception in updating user: '+userdata[0]
                            raise
                else:
                    print 'Ignored invalid record :'+line
            if self._jps_out is not None:
                os.remove(self._jps_out)
        except :
            print 'Exception in updating users...'
            raise

    def delete_groups(self, groups_list):
        if len(groups_list) == 0:
            print 'No group to delete'
            return
        #Display Name,Description,User Members
        print 'Starting deletion of groups ....'
        try:
            wlst.connect(self._user, self._password,'t3://'+socket.getfqdn()+':'+self._port)
            wlst.serverConfig()
            cmo = wlst.cd('/SecurityConfiguration/'+self._domain+'/Realms/myrealm/AuthenticationProviders/DefaultAuthenticator')
            for userdata in groups_list:     # we read all lines into a list and iterate over it
                if len(userdata) >= 1:
                    try:
                        if cmo.groupExists(userdata[0]) :
                            cmo.removeGroup(userdata[0])
                            print 'Removed group: '+ userdata[0]
                        else:
                            print 'Groups does not exists: '+userdata[0]
                    except :
                        print 'Exception in removing group...'+ userdata[0]
                        raise
        except :
            print 'Exception in removing groups...'
            raise

