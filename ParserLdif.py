
class WLSLDIFParser:
    def __init__(self, value):
        self._port = value._port
        self._domain = value._domain
        self._outdir =  value._outdir
        self._user=  value._user
        self._password= value._password
        self._oracle_common = value._oracle_common
        self.group_entries = value.group_entries

        self.mandatory_user_entries = value.mandatory_user_entries
        self.user_entries = self.mandatory_user_entries

        self.group_membership = value.group_membership

    def init_parser(self):
        self.ldif_file = self._outdir + '/wls_users_groups.ldif'
        self.user_file = open(self._outdir+'/users.csv','w')
        self.group_file =  open(self._outdir+'/groups.csv','w')

    def create_group_record(self, args):
        if len(args) == 2:
            args.append('')
        return args[0] + ',' + args[1] + ',' + args[2]

    def write_csv_header(self):
        self.user_file.write('User ID,Last Name,First Name,Display Name,Password,Work Email')
        self.user_file.write('\n')

        self.group_file.write(self.create_group_record(['Display Name','Description','User Members']))
        self.group_file.write('\n')

    def parse(self):
        in_file = open(self.ldif_file, "r")
        record_started = False
        record = {}
        user_record = False
        grp_record = False
        for line in in_file:
            # print 'from file{0}:'.format(line)
            if line.startswith("dn:"):
                if "ou=people,ou=@realm@,dc=@domain@" in line:
                    user_record = True
                    record = self.ger_user_rec_template()
                if "ou=groups,ou=@realm@,dc=@domain@" in line:
                    grp_record = True
                    record = self.ger_group_rec_template()
                record_started = True
            elif len(line.rstrip()) == 0 and (user_record or grp_record):
                record_started = False
                out_writer = None
                #logger.info('parsed one record : {0}'.format(record))
                is_valid = self.is_valid_record(record)
                if user_record and len(record['uid'])  >  0:
                    if is_valid:
                        self.user_file.write(self.create_user_record(record))
                        self.user_file.write('\n')
                        if len(record['members']) > 0:
                            print 'found user-group membership for user ' + record['uid'] + ':'
                            for member in record['members']:
                                csv_rec =  member + ',,' + record['uid'] + '\n'
                                print 'writing user-group membership...' + csv_rec
                                self.add_members_to_group(member, record['uid'])
                    #else:
                        #logger.info("Found invalid user record: "+self.create_user_record(record))
                        #rec_logger.info('INCOMPLETE USER RECORD:' + self.create_user_record(record))
                elif grp_record:
                    if is_valid:
                        self.group_file.write(self.create_group_record([record['cn'], record['description']]))
                        self.group_file.write('\n')
                    else:
                        # out_writer = invalid_user_out
                        print ""
                record = {}
                user_record = False
                grp_record = False
            elif record_started:
                if user_record:
                    key, value = self.parserRecord(line)
                    if key is not None:
                        if key == "wlsMemberOf":
                            record['members'].append(value)
                        else:
                            record[key] = value
                if grp_record:
                    key, value = self.parserRecord(line)

                    if key is not None:
                        if key == "uniquemember":
                            record['members'].append(value)
                        else:
                            record[key] = value
        in_file.close()


    def create_user_record(self,args):
        return args['uid'] + ',' + args['sn'] + ',' + args['givenname'] + ',' + args['displayname'] + ',Welcome1,' + args['mail']


    def ger_user_rec_template(self):
        record = {}
        record["type"] = "user"
        record["uid"] = ''
        record["sn"] = ''
        record["givenname"] = ''
        record["displayname"] = ''
        record["mail"] = ''
        record['members'] = []
        return record

    def ger_group_rec_template(self):
        record = {}
        record["type"] = "group"
        record['members'] = []
        record['description'] = ''
        return record

    def parserRecord(self,line):
        entry = line.split(":")
        key = None
        value = None
        if len(entry) == 2:
            key = entry[0]
            value = entry[1].strip()
            if entry[0] in self.user_entries:
                if key == 'wlsMemberOf':
                    value = self.parse_member(value)
                return key, value
            elif entry[0] in self.group_entries:
                if (key == 'uniquemember'):
                    value = self.parse_member(value)
                    return key, value
        return key, value

    def parse_member(self,member_entry):
        # cn = testgrp1,ou = groups,ou=@realm@,dc=@domain @
        member = None
        member_detail = member_entry.split(',')
        for entry in member_detail:
            if (entry.startswith("cn=")):
                key_value = entry.split('=')
                if len(key_value) == 2:
                    member = key_value[1]
        return member

    def is_valid_record(self,record):
        is_valid = True
        if record['type'] == 'user':
            required_entries = self.mandatory_user_entries
            for entry in required_entries:
                if entry not in record or len(record[entry]) == 0:
                    is_valid = False
        elif record['type'] == 'group':
            if 'cn' not in record:
                is_valid = False
        return is_valid

    def write_group_members(self):
        for group in self.group_membership.keys():
            self.group_file.write( self.create_group_record([ group  ,'',';'.join( self.group_membership[group])]))
            self.group_file.write('\n')

    def add_members_to_group(self, group, member):
        if group in self.group_membership:
            grp_list = self.group_membership[group]
            grp_list.append(member)
            self.group_membership[group] = grp_list
        else:
            self.group_membership[group] = [member]

