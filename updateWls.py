#!/bi/app/python/bin/python

import getopt
import wlstModule as wlst
from WLSLdapUtil import *
from ParserLdif import *
from libCSV import *

OPTION = ['help', 'user=', 'password=', 'domain_home=', 'prod_home=', 'oracle_common=', 'port=', 'in_file=', 'outdir=']
SHORTOPTION = 'hu:w:d:p:c:o:f:D:'
#Provide help for each command
HELP_DICT = {'-h  or --help':'displays script usage help',\
             '--user or -u':'Admin user name string. Mandatory argument',\
             '--password or -w':'Admin password string. Mandatory argument',\
             '--domain_home or -d':'Domain home path. Mandatory argument',\
             '--prod_home or -p':'Product Home path. Mandatory argument',\
             '--oracle_common or -c':'ORACLE_COMMON folder path. Mandatory argument',\
             '--port or -o':'Domain port. Default value is 7001,  Optional argument',\
             '--in_file or -f':'Output director. Mandatory argument',\
             '--outdir or -D':'Input JSON directory with user data. Mandatory argument',\
            }

# Because of different representations of JSON file, the following code
# is used to get on Json file useful item for updating WebLogic Server
codeToGetJson = "def function(item):\n"                     +   \
            "   def get_primary_email(json):\n"         +   \
            "       for email in json['emails']:\n"     +   \
            "           if email['primary'] == True:\n" +   \
            "               return email['value']\n"    +   \
            "       raise Exception('No email found')\n"+   \
            "   row = ''\n" +\
            "   row += get_primary_email(json=item) + ',' +" +\
            "       item['name']['familyName'] + ',' +" +\
            "       item['name']['givenName']  + ',' +" +\
            "       item['name']['formatted']  + ',' +" +\
            "       'Welcome1' + ',' +" +\
            "       get_primary_email(item) \n"  +\
            "   return row"


def displayScriptUsage(helpDict):
    print('  [SCRIPT USAGE]')
    keys = helpDict.keys()
    for key in keys:
        value = helpDict.get(key)
        print('           '+key+' <value>    \t'+value)
    print('\n')
    sys.exit(2)

def parseCommandLine(arguments, longOptions, shortOptions, helpDict):
    try:
       opts, args = getopt.getopt(arguments[1:],shortOptions,longOptions)
       for o,a in opts:
           if o in ('-h','--help'):
               displayScriptUsage(helpDict)
               sys.exit(2)
       return opts
    except getopt.GetoptError, error:
      print error
      WlsLdapUtil.displayScriptUsage(helpDict)
      sys.exit(2)


""" Main function """
""" Check arguments and init clases with it """
options = parseCommandLine(sys.argv, OPTION, SHORTOPTION, HELP_DICT)
ldap_util = WlsLdapUtil(options)
parser = WLSLDIFParser(ldap_util)

""" Read all groups to import and load it """
try:
    group_conf = libCSV(filename='etc/group.csv')
except Exception:
    print("Cannot read group file, can't continue")
    sys.exit(1)

""" Export current user on LDAP Weblogic """
ldap_util.export_ldif()
#Parse ldif file (convert to csv)
parser.init_parser()
parser.write_csv_header()
#Parsing ldif file for users and groups
parser.parse()
##Writing group membership to csv
parser.write_group_members()
#close both the csv files
parser.user_file.close()
parser.group_file.close()

""" Compare users and group on WLS and IDCS """
#Load current user csv file
currentUserCsv = libCSV(filename='current_wls_ldap/users.csv')
#Load current group csv file
currentGroupCsv = libCSV(filename='current_wls_ldap/groups.csv')
#print("Current")
#currentUserCsv._print()

""" Iter on each group and users on 2 lists:
    The first list is for updating user (no roles associate)
    The second list for updating user associate to roles """

template='User ID,Display Name,Password,givenname,lastname,mail'
newCsv = libCSV(template=template)
group_list = {}
for group in group_conf.data:
    if group[0] == '' or group[1] == '':
        continue
    json_directory = 'tmp_' + group[1] + '/'
    group_csv = libCSV(template=template, data=[])
    #Load user information from IDCS to CSV
    for filename in os.listdir(json_directory):
        group_csv.addRowfromJson(codeToGetJson, json_directory + filename)
        newCsv.addSingletonfromJson(codeToGetJson, json_directory + filename)
    group_list.update({group[0]: group_csv})

update_group = libCSV(template='DisplayName,Description,User Members', data=[])
new_group = libCSV(template='DisplayName,Description,User Members', data=[])
delete_group = libCSV(template='DisplayName,Description,User Members', data=[])
list_group = libCSV(template='DisplayName,Description,User Members', data=[])

last_update_group = None
try:
    last_update_group = libCSV(filename='etc/IDCSgroup.csv')
except:
    last_update_group = libCSV(template='DisplayName,Description,User Members', data=[])
    print('No file found, this script will be executed will create the file')

for groupName, Data in group_list.iteritems():
    if groupName not in last_update_group.getListField('DisplayName'):
        print(groupName + " not exist, create group")
        new_group.addData([groupName, groupName, ';'.join(Data.getListField('User ID'))])
    else:
        print("Update user in group: " + groupName)
        update_group.addData([groupName, groupName, ';'.join(Data.getListField('User ID'))])
    list_group.addData([groupName, groupName, ';'.join(Data.getListField('User ID'))])

print("last update")
print(last_update_group.data)
if last_update_group.data != []:
    for groupName in last_update_group.getListField('DisplayName'):
        if groupName not in group_list:
            print(groupName + " not exist anymore, this group will be deleted")
            delete_group.addData([groupName, groupName, ';'.join(Data.getListField('User ID'))])

list_group.writeCsvToFile("etc/IDCSgroup.csv")

#for group in currentGroupCsv.data:
#    try:
#        update_group.addData([group[0], group[1], ';'.join(group_list[group[0]].getListField('User ID'))])
#    except Exception:
#        new_group.addData([group[0], group[1], ''])
#        print('error')
#        print(sys.exc_info()[1])

print("Current User on WebLogic")
currentUserCsv._print()
print("User on IDCS")
newCsv._print()

print("New group detected with following user:")
new_group._print()
print("Following user will be updated in some group:")
update_group._print()
print("Following group will be deleted")
delete_group._print()

same, update, delete, add = compareDetailCSV(libCSV.getDataById(currentUserCsv), libCSV.getDataById(newCsv))
ldap_util.import_users(libCSV.idtolist(add))
ldap_util.delete_users(libCSV.idtolist(delete))
ldap_util.update_users(libCSV.idtolist(update))
#Compare group on WLS and IDCS

ldap_util.import_groups(new_group.data)
ldap_util.update_groups(update_group.data)
ldap_util.delete_groups(delete_group.data)
