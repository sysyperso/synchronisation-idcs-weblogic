import sys

###############################################################################
#                             Generate Random password                        #
###############################################################################
import random

alphabet = "abcdefghijklmnopqrstuvwxyz"
upperalphabet = alphabet.upper()
pw_len = 8
def generatePassword(rand=True):
    if rand == False:
        return 'Welcome1'
    pwlist = []

    for i in range(pw_len//3):
        pwlist.append(alphabet[random.randrange(len(alphabet))])
        pwlist.append(upperalphabet[random.randrange(len(upperalphabet))])
        pwlist.append(str(random.randrange(10)))
    for i in range(pw_len-len(pwlist)):
        pwlist.append(alphabet[random.randrange(len(alphabet))])

    random.shuffle(pwlist)
    return "".join(pwlist)

###############################################################################
#                             Write text in file                              #
###############################################################################

def writeTextToFile(text, filename):
    try:
        f = open(filename, 'w')
        f.write(text)
        f.close()
    except Exception:
        e = sys.exc_info()[1]
        print('error:')
        print(e)
        sys.exit(1)

###############################################################################
#                             Load Json File                                  #
###############################################################################
import sys 
import simplejson as json
#reload(sys)
sys.setdefaultencoding('utf-8')
def loadJsonFile(jsonInput):
    try:
        f = open(jsonInput)
        json_file = json.load(f)
        f.close()
        return json_file
    except ValueError:
        e = sys.exc_info()[1]
        print('Parse json error: ' + str(e))
    except Exception:
        e = sys.exc_info()[1]
        print(e)
    #sys.exit(1)

###############################################################################
#                             Execute code from text                          #
###############################################################################

def Exec(code, arg=None):
    Vars = {}
    result = None
    try:
        exec(str(code)) in Vars
        return Vars['function'](arg)
    except Exception:
        e = sys.exc_info()[1]
        print("Code Error:")
        print(e)
        return "Error"
