#!/bi/app/python/bin/python

import sys
import os
import subprocess
from argparse import ArgumentParser

def perform_import(args):
    app_home = os.path.dirname(os.path.realpath(__file__))
    ret = subprocess.call([os.path.join(args.oracle_common, "common/bin/wlst.sh"),
                           os.path.join(app_home, "updateWls.py"),
                           '--user', args.admin_user,
                           '--password', args.password,
                           '--domain_home', args.domain_home,
                           '--prod_home', args.prod_home,
                           '--oracle_common', args.oracle_common,
                           '--in_file', args.in_file,
                           '--outdir', args.outdir])
    if ret != 0:
        raise Exception("Failed to execute the importation")

def parse_args(input_args):
    parser = ArgumentParser(description="Update users or groups on WebLogic from IDCS",
                            epilog="This script work only if the following argument is present: "
                                   "[User ID, Display Name, Password]. ")

    parser._action_groups.pop()
    required = parser.add_argument_group('Mandatory arguments')
    required.add_argument("--admin-user", help="Weblogic admin user name", required=True)
    required.add_argument("--filename", help="CSV file name")
    required.add_argument("--password", help="Admin password string. Mandatory argument", required=True)
    required.add_argument("--domain_home", help="Domain home path. Mandatory argument", required=True)
    required.add_argument("--prod_home", help="Product Home path. Mandatory argument", required=True)
    required.add_argument("--oracle_common", help="ORACLE_COMMON folder path. Mandatory argument", required=True)

    required.add_argument("--in_file", help="Output director. Mandatory argument", required=True)
    required.add_argument("--outdir", help="Input JSON directory with user data. Mandatory argument", required=True)

    required = parser.add_argument_group('Optional arguments')
    required.add_argument("--port", help="Domain port. Default value is 7001,  Optional argument", required=False)
    #configure_logging.add_parser_options(parser)

    args = parser.parse_args(input_args)

    return args


def main(arguments):
    #check_user_is_install_user()
    args = parse_args(arguments)
    #configure_logging.do_config(args)
    perform_import(args)

if __name__ == '__main__':
    main(sys.argv[1:])
"""def main():
    options = parseCommandLine(sys.argv, OPTION, SHORTOPTION, HELP_DICT)
    ldap_util = WlsLdapUtil(options)
    parser = WLSLDIFParser(ldap_util)
    #Get current user on WLS
    #ldap_util.export_ldif()
        #Parse ldif file (convert to csv)
    parser.init_parser()
    parser.write_csv_header()
        #Parsing ldif file for users and groups
    parser.parse()
        #Writing group membership to csv
    parser.write_group_members()
        #close both the csv files
    parser.user_file.close()
    parser.group_file.close()
    #Compare users on WLS and IDCS
        #Load current user csv file
    currentCsv = libCSV(filename='current_wls_ldap/users.csv')
        #print("Current")
        #currentCsv._print()
    json_directory = 'tmp_user_json/'
    template='User ID,Display Name,Password,givenname,lastname,mail'
        #Load user information from IDCS to CSV
    newCsv = libCSV(template=template)
    for filename in os.listdir(json_directory):
        newCsv.addRowfromJson(codeToGetJson, json_directory + filename)
        #print("New list")
        #newCsv._print()
    same, update, delete, add = libCSV.compareCSV(libCSV.getDataById(currentCsv), libCSV.getDataById(newCsv))

    sys.exit(1)

    #Update from each list on WLS

    #Disconnect

if __name__ == "__main__":
    main()"""
