#!/bi/app/python/bin/python

import os
#import csv

from utils import writeTextToFile, loadJsonFile, Exec 

class libCSV:
    def _checkCsvRow(self, row):
        fields = row.split(self.separator)
        return fields, len(fields)

    def addRowfromJson(self, code, jsonFile):
        item = loadJsonFile(jsonFile)
        try:
            fields, nbValue = self._checkCsvRow(Exec(code, arg=item))
            if nbValue != self.nbFields:
                raise ValueError("Found " + str(nbValue) +               \
                                    "value in the line ")
            else:
                self.data += [fields]
        except ValueError:
            print("Numbers of fields different from the template")


    def addSingletonfromJson(self, code, jsonFile):
        item = loadJsonFile(jsonFile)
        try:
            fields, nbValue = self._checkCsvRow(Exec(code, arg=item))
            if nbValue != self.nbFields:
                raise ValueError("Found " + str(nbValue) +               \
                                    "value in the line ") #+ Rows[a])
            else:
                if fields not in self.data:
                    self.data += [fields]
        except ValueError:
            print("Numbers of fields different from the template")

    def loadFromCsvFile(self, filename):
        try:
            f = open(filename)
            csv_file = f.read().split('\n')
            #csv_file = csv.reader(f, delimiter=',')
            self.template = csv_file[0].split(',')
            if self.template == None:
               raise ValueError("CSV file is empty")
            self.data = []
            for row in csv_file[1:]:
                if row:
                    self.data += [row.split(',')]
        except ValueError:
            e = sys.exc_info()[1]
            print('Parse csv error: ' + str(e))
        except Exception:
            e = sys.exc_info()[1]
            print(e)

    def __init__(self, template=None, data=[], filename=None, separator=","):
        self.separator = separator
        if filename != None:
            self.loadFromCsvFile(filename)
            return
        if template != None:
            self.template, self.nbFields= self._checkCsvRow(template)
        else:
            print("You have to define a template for instantiate this class")
            raise Execption
        if data != []:
            print(data)
            Rows = data.split(os.linesep)
            self.data = []
            try:
                for a in range(0, len(Rows)):
                    fields, nbValue = self._checkCsvRow(Rows[a])
                    if nbValue != self.nbFields:
                        raise ValueError("Found " + nbValue +
                                               "value in the line " + Rows[a])
                    else:
                        self.data += [fields]
            except ValueError:
                print("Numbers of fields different from the template")
        else:
            self.data = data

    def addData(self, row):
        if len(row) is self.nbFields: 
            self.data += [row]
            return
        print("Cannot add data")

    def _getText(self):
        try:
            content = ','.join(self.template) + os.linesep
            for row in self.data:
                content += ','.join(row) + os.linesep
            return content
        except Exception:
            e = sys.exc_info()[1]
            print(e)
            print("Class is set but no data found")
            return ""

    def writeCsvToFile(self, filename):
       writeTextToFile(self._getText(), filename)

    def _print(self):
        print(self._getText())

    def getData(self):
        return self.data

    def getListField(self,fieldName):
        try:
            index = self.template.index(fieldName)
        except:
            print("Field " + fieldName + " not found")
            return
        list_data_field = []
        for row in self.data:
            list_data_field += [row[index]]
        return list_data_field


###############################################################################
#                            Convert csv to list                              #
#                       csv file:id1,arg1,arg2,arg3                           #
#                      ___    _____________                                   #
#                     |id1|  |arg1|arg2|arg3|                                 #
#                     |___|->|____|____|____|                                 #
#                                                                             #
###############################################################################
#    @staticmethod
    def getDataById(self):
        l1 = []
        for row in self.data:
            l2 = [row[0]]
            l3 = []
            for elt in row[1:]:
                l3 += [elt]
            l2 += [l3]
            l1 += [l2]
        return l1

#    @staticmethod
    def idtolist(idList):
        l1 = []
        for row in idList:
            l2 = []
            l2.append(row[0]) 
            for arg in row[1:]:
                l2 += arg
            l1 += [l2]
        return l1

    idtolist = staticmethod(idtolist)


def compareDetailCSV(old_list, new_list):
    same_value = []
    update_value = []
    delete_value = []

    hadInsert=False
    add_value = new_list
    for id1, detail1 in old_list:
        for id2, detail2 in new_list:
            if id1 == id2:
                if detail1 == detail2:
                    same_value += [[id1, detail1]]
                    add_value.remove([id1, detail1])
                else:
                    update_value += [[id2, detail2]]
                    add_value.remove([id2, detail2])
                hadInsert=True
                break
        if hadInsert == True:
            hadInsert = False
        else:
            delete_value += [[id1, detail1]]

    return same_value, update_value, delete_value, add_value

def compareList(old_list, new_list):
    update_list = []
    for elt1 in old_list:
        if elt1 not in new_list:
            update_list += [elt1]
    return update_list
